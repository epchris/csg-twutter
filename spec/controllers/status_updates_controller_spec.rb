require 'rails_helper'
RSpec.describe StatusUpdatesController, type: :request do

  describe '#create' do
    let(:body_text) { "hello world" }
    it 'creates a new update' do
      post "/status_updates", params: {status_update: {body: body_text}}
      expect(response).to have_http_status(:redirect)

      new_update = StatusUpdate.where(body: body_text).last
      expect(new_update).to be
      expect(new_update.body).to eq(body_text)
    end
  end

  describe '#index' do
    before do
      20.times do
        date = Faker::Time.between(2.days.ago, Date.today, :evening)
        StatusUpdate.create(body: Faker::ChuckNorris.fact, created_at: date, updated_at: date )
      end
    end

    it 'returns updates in reverse cronological order'
  end
end
