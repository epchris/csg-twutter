# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
20.times do
  date = Faker::Time.between(2.days.ago, Date.today, :evening)
  StatusUpdate.create(body: Faker::ChuckNorris.fact, created_at: date, updated_at: date )
end
