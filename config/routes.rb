Rails.application.routes.draw do
  resources :status_updates
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root controller: 'status_updates', action: 'index'
end
